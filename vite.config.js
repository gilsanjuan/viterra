import path from 'path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue2';
import svgLoader from 'vite-svg-loader'
// import svgLoader from 'vue-svg-inline-loader';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
        svgLoader({
            defaultImport: 'url' // or 'raw'
        }),
        // svgLoader(),
        vue(),
        // vue({
        //     template: {
        //         transformAssetUrls: {
        //             base: null,
        //             includeAbsolute: false,
        //         },
        //     },
        // }),
    ],
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, '/node_modules/bootstrap'),
            '~@fortawesome': path.resolve(__dirname, '/node_modules/@fortawesome'),
            '~bootstrap-slider': path.resolve(__dirname, '/node_modules/bootstrap-slider'),
            '~animate.css': path.resolve(__dirname, '/node_modules/animate.css'),
            '~vue-spinner': path.resolve(__dirname, '/node_modules/vue-spinner'),
            vue: 'vue/dist/vue.esm.js',
            '@': '/resources/js'
        },
    },
    server: {
        hmr: {
            host: 'localhost',
        },
    },
    build: {
        rollupOptions: {
            output: {
                assetFileNames: (assetInfo) => {
                    // Get file extension
                    // TS shows asset name can be undefined so I'll check it and create directory named `compiled` just to be safe
                    let extension = assetInfo.name?.split('.').at(1) ?? 'compiled'

                    // This is optional but may be useful (I use it a lot)
                    // All images (png, jpg, etc) will be compiled within `images` directory,
                    // all svg files within `icons` directory
                    // if (/png|jpe?g|gif|tiff|bmp|ico/i.test(extension)) {
                    //     extension = 'images'
                    // }

                    // if (/svg/i.test(extension)) {
                    //     extension = 'icons'
                    // }

                    // Basically this is CSS output (in your case)
                    return `${extension}/[name].[hash][extname]`
                },
                chunkFileNames: 'js/chunks/[name].[hash].js', // all chunks output path
                entryFileNames: 'js/[name].[hash].js', // all entrypoints output path
                manualChunks(id) {
                    if (id.includes('node_modules')) {
                        return id.toString().split('node_modules/')[1].split('/')[0].toString();
                    }
                }
                // manualChunks(id) {
                //     if (id.includes('node_modules')) {
                //         return 'vendor';
                //     }
                // }
            }
        }
    }
});
