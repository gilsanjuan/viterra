/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';

import Vue from 'vue';
import store from './store';
import router from './router';
import Meta from 'vue-meta'
import SvgIcon from 'vue-svgicon';
import VeeValidate from 'vee-validate';
import { i18n } from './plugins/i18n.js';
import Notifications from 'vue-notification';
import bFormSlider from 'vue-bootstrap-slider';
// import * as VueGoogleMaps from 'vue2-google-maps'
import GmapVue from 'gmap-vue';
// import { InlineSvgPlugin } from 'vue-img';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const MAPS_OPTIONS = {
    load: {
        // key: process.env.MIX_GOOGLE_MAPS_KEY,
        key: import.meta.env.VITE_GOOGLE_MAPS_KEY,
        libraries: 'places',
        region: 'MX',
        language: 'es-419'
    },
    // autoBindAllEvents: false,
    // installComponents: true,
    // dynamicLoad: false,
};

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.use(Meta, {
    keyName: 'metaInfo',
    attribute: 'data-vue-meta',
    ssrAttribute: 'data-vue-meta-server-rendered',
    tagIDKeyName: 'vmid',
    refreshOnceOnNavigation: true
});
Vue.use(SvgIcon, {
    tagName: 'svgicon'
});
// Vue.use(InlineSvgPlugin);
Vue.use(Notifications);
Vue.use(GmapVue, MAPS_OPTIONS);
Vue.use(bFormSlider);
Vue.use(VeeValidate);

const app = new Vue({
    store,
    router,
    i18n,
    metaInfo: {
        title: 'Inicio',
        titleTemplate: 'Viterra Grupo Inmobiliario | %s',
        meta: [
            { name: 'description', content: 'Empresa inmobiliaria con más de 60 años en el mercado, enfocada principalmente al mercado residencial alto, Contamos con un corporativo en Guadalajara, oficinas en Puerto Vallarta, Cancún, Rivera Maya y Querétaro. También operamos con asesores en Jalisco, Cd. De México y área conurbada, Aguascalientes y Monterrey.' }
        ]
    },
    mounted() {
        this.$i18n.locale = 'es';
        this.$store.dispatch('language/setLanguage', 'es');
    }
}).$mount('#app');
