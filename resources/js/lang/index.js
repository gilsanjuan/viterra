import es from './locales/es';
import en from './locales/en';

export default {
    en,
    es
}
