import Vue from 'vue';
import Vuex from 'vuex';

//Modules
import language from './modules/language';
import search from  './modules/properties'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        language,
        search
    }
});

export default store;
