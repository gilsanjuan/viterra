import Vue from 'vue';

export default {
    strict: true,
    state: {
        search: {
            place: null,
            typeProperty: null,
            typeTrade: null,
            home: 0
        }
    },
    getters: {
        search: state => state.search
    },
    mutations: {
        update_search(state, search) {
            Vue.set(state, 'search', search);
        }
    },
    actions: {
        updateSearch({ commit }, search) {
            if (search) {
                commit('update_search', search);
            }
        }
    }
}
