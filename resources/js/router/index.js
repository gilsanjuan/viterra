import Vue from 'vue';
import store from '../store';
import Router from 'vue-router';
import {i18n} from '../plugins/i18n';
import {constantRoutes} from './modules/app';

Vue.use(Router);

const createRouter = () => new Router({
    mode: 'history',
    scrollBehavior: () => ({y: 0}),
    routes: constantRoutes,
});

const router = createRouter();

router.beforeEach((to, from, next) => {
    if (store.state.language.language && store.state.language.language !== i18n.locale) {
        i18n.locale = store.state.language.language;
        next();
    } else if (!store.state.language.language) {
        store.dispatch('language/setLanguage', navigator.languages)
            .then(() => {
                i18n.locale = store.state.language.language;
                next();
            });
    } else {
        next();
    }
});

export default router;
