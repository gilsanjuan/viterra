
export const constantRoutes = [
    {
        path: '/',
        component: () => import('@/views/Home.vue'),
        name: 'home'
    },
    {
        path: '/about-us',
        component: () => import('@/views/AboutUs.vue'),
        name: 'about-us'
    },
    {
        path: '/contact',
        component: () => import('@/views/Contact.vue'),
        name: 'contact'
    },
    {
        path: '/search',
        component: () => import('@/views/SearchView.vue'),
        name: 'search'
    },
    {
        path: '/our-team',
        component: () => import('@/views/OurTeam.vue'),
        name: 'our-team'
    },
    {
        path: '/property/:city/:tradetype/:propertytype/:slug',
        name: 'property',
        component: () => import('@/views/Property.vue'),
        props: true
    },
    {
        path: '/developments',
        name: 'developments',
        component: () => import('@/views/Developments.vue')
    },
    {
        path: '/development/:slug',
        name: 'development',
        component: () => import('@/views/Development.vue'),
        props: true
    },
    {
        path: '/404',
        name: 'error_404',
        component: () => import('@/views/errors/404.vue')
    },
    {
        path: '*',
        redirect: '/404',
    }
];
