@extends('layouts.layout')
@section('content')
    <router-view :key="$route.fullPath"></router-view>
    <notifications position="top right" group="foo"/>
@endsection
