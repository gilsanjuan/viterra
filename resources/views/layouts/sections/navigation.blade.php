<header id="header" class="bg-dark">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="#">
                <img src="{{asset('img/logo.png')}}" alt="logo-viterra">
            </a>
            <a class="btn link-spanglish ml-auto" href="#">
                ES/EN
            </a>
        </nav>
    </div>
</header>
