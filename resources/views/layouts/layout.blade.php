<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="w-100">
@include('layouts.sections.head')
<body class="h-100 w-100">
<div id="app" class="w-100 h-100">
    @yield('content')
</div>
@include('layouts.sections.scripts')
<!-- Start of viterrainmobiliaria Zendesk Widget script -->
<!-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=8a5e967e-fa7e-439a-b170-3bc6998e540e"> </script> -->
<!-- End of viterrainmobiliaria Zendesk Widget script -->
</body>
</html>
