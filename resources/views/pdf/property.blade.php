<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Viterra</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset(mix('css/app.css'))}}">
    <link rel="stylesheet" href="{{asset(mix('css/styles.css'))}}">
    <style>
        .bg-title {
            background-color: #efe9e9;
            color: #000000;
        }

        .etiqueta {
            position: absolute;
            right: 110pt;
            top: -40pt;
            z-index: 1;
        }

        .trade-type {
            position: relative;
            right: -30pt;
            top: -55pt;
            z-index: 3;
            transform: rotate(-90deg);
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }

        table {
            table-layout: fixed;
        }

        .border {
            border: 1px solid #dee2e6 !important;
            border-top-color: rgb(222, 226, 230);
            border-right-color: rgb(222, 226, 230);
            border-bottom-color: rgb(222, 226, 230);
            border-left-color: rgb(222, 226, 230);
        }

        .border-danger {
            border-color: #e3342f !important;
        }

        .border-dark {
            border-color: #343a40 !important;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="bg-title" style="font-size: 0.8rem">
        <table class="table table-sm table-borderless" width="100%">
            <tr>
                <td class="text-center text-uppercase" width="66.6%">
                    <h1>{{$property->name}}</h1>
                </td>
                <td class="etiqueta" width="33.3%">
                    <img src="{{asset('img/etiqueta-ficha.jpg')}}" alt="etiqueta" class="etiqueta">
                    <div class="trade-type">
                        <h4 class="text-uppercase text-white m-0">{{$property->tradeType->name}}</h4>
                        <p class="text-uppercase text-white m-0"><small>{{$property->propertyType->name}}</small></p>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table table-sm table-borderless" width="100%">
            <tr>
                <td class="text-center border-right border-top border-danger" width="33.3%">
                    <img src="{{asset('img/logo-pdf.jpg')}}" alt="logo" width="90px" class="pt-2">
                </td>
                <td class="text-center text-uppercase border-right border-top border-danger" width="33.3">
                    {{$property->location->address}}
                </td>
                <td class="text-uppercase text-center" width="33.3">
                    <p>{{$property->tradeType->name}}: ${{number_format($property->price, 2)}} MXN</p>
                    @if($property->maintenance)
                        <p>mantenimiento:</p>
                        <p>{{$property->maintenance}}</p>
                    @endif
                </td>
            </tr>
        </table>
    </div>
    <div class="border border-danger">
        <table class="table table-sm table-borderless" width="100%">
            <caption class="text-center text-uppercase"><h3 class="text-dark">descripción general</h3></caption>
            <tr>
                <td>
                    <table class="table table-sm table-borderless" width="100%">
                        <tr>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>tipo propiedad</strong></h6>
                                @if($property->propertyType)
                                    <p>{{$property->propertyType->name}}</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>arquitectura</strong></h6>
                                @if($property->architecture_type)
                                    <p>{{$property->architecture_type}}</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>uso suelo</strong></h6>
                                @if($property->land_use)
                                    <p>{{$property->land_use}}</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>antiguedad</strong></h6>
                                @if($property->antique)
                                    <p>{{$property->antique}}</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="33.3%">
                                <h6><strong>jardin</strong></h6>
                                @if($property->garden)
                                    <p>{{$property->garden_square_meters}}</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table table-sm table-borderless" width="100%">
                        <tr>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>mts terreno</strong></h6>
                                @if($property->land_square_meters)
                                    <p>{{$property->land_square_meters}} M2</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>mts construccion</strong></h6>
                                @if($property->square_meters)
                                    <p>{{$property->square_meters}} M2</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>frente</strong></h6>
                                @if($property->front)
                                    <p>{{$property->front}}</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>fondo</strong></h6>
                                @if($property->bottom)
                                    <p>{{$property->bottom}}</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>pisos</strong></h6>
                                <p>{{$property->floor}}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table table-sm table-borderless" width="100">
                        <tr>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>recámaras</strong></h6>
                                <p>{{$property->room}}</p>
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>baños</strong></h6>
                                <p>{{$property->bathroom}}</p>
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>salon tv</strong></h6>
                                @if($property->tv_salon)
                                    <p>SI</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>estudio</strong></h6>
                                @if($property->studio)
                                    <p>SI</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>pisos</strong></h6>
                                <p>{{$property->floor}}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table table-sm table-borderless" width="100%">
                        <tr>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>estacionamientos</strong></h6>
                                <p>{{$property->car_spot}}</p>
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>terraza</strong></h6>
                                @if($property->terrace)
                                    <p>SI</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>coto</strong></h6>
                                @if($property->residential_development)
                                    <p>SI</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>vigilancia</strong></h6>
                                @if($property->vigilance)
                                    <p>SI</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                            <td class="text-center text-uppercase" width="20%">
                                <h6><strong>marcotas</strong></h6>
                                @if($property->pet)
                                    <p>SI</p>
                                @else
                                    <p>NO</p>
                                @endif
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="border border-dark">
                    <strong>DESCRIPCIÓN:</strong><br>
                    {!! $property->description !!}
                </td>
            </tr>
        </table>
        @if($property->images)
            <table class="table table-sm table-borderless" width="100%">
                @for($i = 0; $i < count($property->images->gallery); $i++)
                    <tr>
                        <td>
                            <img src="{{asset($property->images->gallery[$i])}}" alt="imagen" width="300px">
                        </td>
                        @if(($i + 1) < count($property->images->gallery))
                            <td>
                                <img src="{{asset($property->images->gallery[++$i])}}" alt="imagen" width="300px">
                            </td>
                        @endif
                    </tr>
                @endfor
            </table>
        @endif
    </div>
</div>
</body>
</html>
