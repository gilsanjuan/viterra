<?php
return [
    'property'           => 'Property|Properties',
    'property_link_text' => 'View all properties',
    'property_text'      => 'You have :count :string in your database. Click on button below to view all properties.'
];
