<?php
return [
    'property'           => 'Propiedad|Propiedades',
    'property_link_text' => 'Ver todas las propiedades',
    'property_text'      => 'Tiene :count :string en su base de datos. Haga clic en el botón de abajo para ver todas las propiedades. '
];
