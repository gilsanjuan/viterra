<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('advantage_development', function (Blueprint $table) {
            $table->unsignedBigInteger('advantage_id')->nullable();
            $table->unsignedBigInteger('development_id');

            $table->foreign('development_id')->references('id')->on('developments')->onDelete('cascade');
            $table->foreign('advantage_id')->references('id')->on('advantages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('advantage_development');
    }
};
