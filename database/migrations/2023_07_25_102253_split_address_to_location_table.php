<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->string('street', 100)->nullable()->after('address');
            $table->string('no_ext', 100)->nullable()->after('address');
            $table->string('no_int', 100)->nullable()->after('address');
            $table->string('suburb', 100)->nullable()->after('address');
            $table->string('state', 100)->nullable()->after('address');
            $table->string('town', 100)->nullable()->after('address');
            $table->string('zip', 100)->nullable()->after('address');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('location', function (Blueprint $table) {
            Schema::drop('street');
            Schema::drop('no_ext');
            Schema::drop('no_int');
            Schema::drop('suburb');
            Schema::drop('state');
            Schema::drop('town');
            Schema::drop('zip');
        });
    }
};
