<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('slug', 100);
            $table->integer('floor')->nullable();
            $table->integer('room')->nullable();
            $table->decimal('bathroom')->default(0);
            $table->integer('car_spot')->default(0);
            $table->decimal('price')->nullable();
            $table->text('description')->nullable();
            $table->string('contact', 100)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('email', 100)->nullable();

            $table->unsignedBigInteger('location_id')->nullable();
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->unsignedBigInteger('trade_type_id')->nullable();
            $table->unsignedBigInteger('property_type_id')->nullable();

            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('set null');
            $table->foreign('trade_type_id')->references('id')->on('trade_types')->onDelete('set null');
            $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
