<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('developments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->double('price');
            $table->string('address');
            $table->text('description');
            $table->text('image')->nullable();
            $table->text('banner')->nullable();
            $table->point('location')->nullable();
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->text('brochure')->nullable();
            $table->text('cover_video')->nullable();
            $table->text('youtube_video')->nullable();
            $table->text('price_file')->nullable();
            $table->timestamps();

            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('developments');
    }
};
