<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->double('square_meters')->nullable()->after('email');
            $table->string('visit_conditions', 100)->nullable()->after('email');
            $table->string('land_use', 100)->nullable()->after('email');
            $table->double('front')->nullable()->after('email');
            $table->double('bottom')->nullable()->after('email');
            $table->boolean('garden')->default(false)->after('email');
            $table->double('garden_square_meters')->nullable()->after('email');
            $table->string('antique', 100)->nullable()->after('email');
            $table->boolean('residential_development')->default(false)->after('email');
            $table->boolean('vigilance')->default(false)->after('email');
            $table->boolean('terrace')->default(false)->after('email');
            $table->boolean('tv_salon')->default(false)->after('email');
            $table->boolean('studio')->default(false)->after('email');
            $table->boolean('furnished')->default(false)->after('email');
            $table->string('another', 100)->nullable()->after('email');
            $table->string('architecture_type', 100)->nullable()->after('email');
            $table->string('maintenance', 100)->nullable()->after('email');
            $table->boolean('room_service')->default(false)->after('email');
            $table->boolean('bathroom_service')->default(false)->after('email');
            $table->double('land_square_meters')->nullable()->after('email');
            $table->text('colony_description')->nullable()->after('email');
            $table->text('nearby_services')->nullable()->after('email');
            $table->boolean('pet')->default(false)->after('email');
            $table->string('optioner', 100)->nullable()->after('email');

            $table->boolean('feature')->default(false)->after('email');
            $table->string('cover_video', 191)->nullable()->after('email');
            $table->text('youtube_video')->nullable()->after('email');
            $table->string('key_words', 100)->nullable()->after('email');
            $table->text('virtual_tour')->nullable()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('square_meters');
            $table->dropColumn('visit_conditions');
            $table->dropColumn('land_use');
            $table->dropColumn('front');
            $table->dropColumn('bottom');
            $table->dropColumn('garden');
            $table->dropColumn('garden_square_meters');
            $table->dropColumn('antique');
            $table->dropColumn('residential_development');
            $table->dropColumn('vigilance');
            $table->dropColumn('terrace');
            $table->dropColumn('tv_salon');
            $table->dropColumn('studio');
            $table->dropColumn('furnished');
            $table->dropColumn('another');
            $table->dropColumn('architecture_type');
            $table->dropColumn('maintenance');
            $table->dropColumn('room_service');
            $table->dropColumn('bathroom_service');
            $table->dropColumn('land_square_meters');
            $table->dropColumn('colony_description');
            $table->dropColumn('nearby_services');
            $table->dropColumn('pet');
            $table->dropColumn('optioner');

            $table->dropColumn('feature');
            $table->dropColumn('slug');
            $table->dropColumn('cover_video');
            $table->dropColumn('youtube_video');
            $table->dropColumn('key_words');
            $table->dropColumn('virtual_tour');
        });
    }
};
