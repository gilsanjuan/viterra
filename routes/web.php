<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

use App\Http\Controllers\CityController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\TradeTypeController;
use App\Http\Controllers\PropertyTypeController;
use App\Http\Controllers\DevelopmentController;
use App\Http\Controllers\HomeSlideImageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('property-pdf/{id}', [PropertyController::class, 'downloadPDF'])->name('download.pdf');
// [::class]
Route::prefix('api')->group(function(){
    Route::get('cities', [CityController::class, 'getCities']);
    Route::post('contacts', [ContactController::class, 'store']);
    Route::get('property', [PropertyController::class, 'show']);
    Route::get('featured', [PropertyController::class, 'getFeatured']);
    Route::get('properties',  [PropertyController::class, 'getProperties']);
    Route::get('trade-types', [TradeTypeController::class, 'getTradeTypes']);
    Route::get('filter-properties', [PropertyController::class, 'filterProperties']);
    Route::get('filter-properties-home', [PropertyController::class, 'getPropertiesCity']);
    Route::get('property-types', [PropertyTypeController::class, 'getPropertyTypes']);
    Route::get('money', [PropertyController::class, 'getMoney']);
    Route::apiResource('development', DevelopmentController::class)->only(['index']);
    Route::post('development', [DevelopmentController::class, 'show'])->name('development.show');
    Route::get('development-featured', [DevelopmentController::class, 'getFeatured'])->name('development.featured');
    Route::get('slide-images', [HomeSlideImageController::class, 'index'])->name('slide.images');
});

Route::middleware('web')->group(function(){
    Route::get('/{any}', [HomeController::class,'index'])->where('any', '.*');
});
