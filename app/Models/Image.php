<?php

namespace App\Models;

use Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'property_id'
    ];

    protected $appends = [
      'gallery'
    ];

    public function getGalleryAttribute(){
      $images = json_decode($this->name, true);
      foreach ($images as $key => $image) {
         $url = Storage::url($image);
         $images[$key] = $url;
      }
      return $images;
    }

    public function property()
    {
      return $this->belongsToMany(Property::class);
    }
}
