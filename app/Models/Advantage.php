<?php

namespace App\Models;

use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Advantage extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'icon'
    ];

    protected $appends = [
        'image'
    ];

    public function getImageAttribute()
    {
        if ($this->icon) {
            return Storage::url($this->icon);
        } else {
            return null;
        }
    }

    public function developments(): BelongsToMany
    {
        return $this->belongsToMany(Development::class);
    }
}
