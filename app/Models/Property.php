<?php

namespace App\Models;

use Storage;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Property extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'name', 'slug', 'floor', 'room', 'bathroom', 'car_spot', 'price', 'description', 'contact', 'phone', 'email', 
        'trade_type_id', 'property_type_id',
        'square_meters', 'visit_conditions', 'land_use',  'front', 'bottom', 'garden', 'garden_square_meters', 'antique', 'residential_development', 
        'vigilance', 'terrace', 'tv_salon', 'studio', 'furnished', 'another', 'architecture_type', 'maintenance', 'room_service', 'bathroom_service',
        'land_square_meters', 'colony_description', 'nearby_services', 'pet', 'optioner',
        'feature', 'youtube_video', 'key_words', 'virtual_tour',
        'short_description',  'levels', 
        'seller_id','ubication_id',
    ];

    protected $appends = [
        'city_slug', 'image_video',
        // 'trade_type_slug', 'property_type_slug',
    ];

    protected $hidden = [
        'cover_video',
    ];

    public function tradeType()
    {
        return $this->belongsTo(TradeType::class);
    }

    public function propertyType()
    {
        return $this->belongsTo(PropertyType::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    // public function images()
    // {
    //     return $this->hasOne(Image::class);
    // }


    public function sort_objects_by_total($a, $b) {
        if($a->order_column == $b->order_column){ return 0 ; }
        return ($a->order_column < $b->order_column) ? -1 : 1;
    }

    public function getImagesAttribute()
    {
        $images = [];
        $media = $this->getMedia('properties')->map( function($mediaObject) {
            // dd($mediaObject->getResponsiveImageUrls());
            // return $mediaObject->attributesToArray();
            return $mediaObject->getFullUrl();
            // return $mediaObject->getResponsiveImageUrls();
            // return $mediaObject;
        });   
        // $media->sortBy('order_column');

        // foreach ($media as $value) {
        //     $images[] = $value['original_url'];
        // }

        // return $images;
        return $media;
    }


    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function getCitySlugAttribute()
    {
        if(!empty($this->location->city))
        {
            return Str::slug($this->location->city->name, '-');
        }
        else {
            return null;
        }

    }

    public function getImageVideoAttribute()
    {
        if ($this->cover_video) {
            return Storage::url($this->cover_video);
        }
        return null;
    }

    public function getTradeTypeSlugAttribute()
    {
        if(!empty($this->tradeType))
        {
            return Str::slug($this->tradeType->name, '-');
        }
        else {
            return null;
        }
    }

    public function getPropertyTypeSlugAttribute()
    {

        if(!empty($this->propertyType))
        {
            return Str::slug($this->propertyType->name, '-');
        }
        else {
            return null;
        }
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb_fit')
            ->fit(Manipulations::FIT_MAX, 100, 100)
            ->nonQueued();

        $this->addMediaConversion('small')
            // ->width(480)
            // ->height(270);
            ->fit(Manipulations::FIT_MAX, 480, 270)
            // ->fit(Manipulations::FIT_CROP, 455, 336)
            ->nonQueued();

        $this->addMediaConversion('medium')
            // ->width(1200)
            // ->height(675);
            ->fit(Manipulations::FIT_MAX, 1010, 480)
            ->nonQueued();

        $this->addMediaConversion('hight')
            // ->width(1920)
            // ->height(1080);
            ->fit(Manipulations::FIT_MAX, 1920, 1080)
            ->nonQueued();

        // $this->addMediaConversion('responsive')
        //     ->withResponsiveImages();
    }
}
