<?php

namespace App\Models;

// use TCG\Voyager\Traits\Spatial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use MatanYadaev\EloquentSpatial\SpatialBuilder;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Traits\HasSpatial;

class Location extends Model
{
    use HasFactory;
    use HasSpatial;

    protected $fillable = [
        'address', 'lat_lng', 'type', 'city_id',
        'street', 'no_ext', 'no_int',
        'suburb', 'state','town', 'zip'
    ];

    // protected $spatial = ['lat_lng'];

    protected $appends = ['coordinates', 'locator'];

    protected $hidden = ['lat_lng'];
// 
    protected $casts = [
        'lat_lng' => Point::class,
    ];

    /**
     * property relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * city relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * extract coordinates from locations
     *
     * @return string
     */
    public function getCoordinatesAttribute()
    {
        $coords = $this->lat_lng;

        if (!$coords->latitude || !$coords->longitude) {
            return sprintf('%s,%s', env('GOOGLE_MAPS_DEFAULT_CENTER_LAT'), env('GOOGLE_MAPS_DEFAULT_CENTER_LNG'));
        } else {
            return sprintf('%s,%s', $coords->latitude, $coords->longitude);
        }
    }

     /**
     * extract locator from locations
     *
     * @return string
     */
     public function getLocatorAttribute()
     {
         $coords = $this->lat_lng;
 
         if (isset($coords->latitude) && isset($coords->longitude)) {
             return json_encode([ 'lat' => $coords->latitude, 'lng' => $coords->longitude]);
         } else {
             return json_encode(['lat' => env('GOOGLE_MAPS_DEFAULT_CENTER_LAT'), 'lng' => env('GOOGLE_MAPS_DEFAULT_CENTER_LNG')]);
         }
     }
 
    public function saveLatLng($location)
    {
        $parseLocation = preg_split ("/\,/", $location);
        $this->location = new Point($parseLocation[0], $parseLocation[1]);
        $this->save();
    }

    protected function serializeLatLng($location)
    {
        $parseLocation = preg_split ("/\,/", $location);
        return  new Point($parseLocation[0], $parseLocation[1]);
    }

}
