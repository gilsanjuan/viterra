<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;
    protected $fillable = [
        'name'
    ];

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function developments()
    {
        return $this->hasMany(Development::class);
    }
}
