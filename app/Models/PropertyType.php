<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Support\Facades\App;

class PropertyType extends Model
{
    use HasFactory;
    use HasTranslations;

    protected $fillable = [
      'name'
    ];

    public $translatable = [
        'name'
    ];

    // public function getNameAttribute() {
    //     $name = $this->getTranslations('name');
    //     return $name[ App::getLocale()];
    // }

    public function property()
    {
      return $this->hasOne(Property::class);
    }
}
