<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Storage;
use MatanYadaev\EloquentSpatial\SpatialBuilder;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Traits\HasSpatial;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Casts\Attribute;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Development extends Model implements HasMedia
{
    use HasFactory, HasSpatial, InteractsWithMedia;

    protected $fillable = [
        'name', 'slug', 'address', 'image', 'banner', 'brochure', 'price_file',
        'galery', 'description', 'price', 'location', 'currency_id',
        'cover_video', 'youtube_video',
    ];

    protected $appends = [
        'cover', 'bigImage', 'images', 'coordinates', 'image_video', 'gallery_media', 
        'file', 'file_price', 'locator'
    ];

    protected $hidden = ['location', 'galery', 'created_at', 'updated_at'];

    protected $casts = [
        'location' => Point::class,
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function advantages(): BelongsToMany
    {
        return $this->belongsToMany(Advantage::class);
    }

    public function getGalleryMediaAttribute() {
        $media = $this->getMedia('developments');
        $mediaResponsive = [];
        foreach ($media as $key => $image) {
            $mediaResponsive[$key] = $image->getUrl();
        }
        return $mediaResponsive;
    }

    public function getCoverAttribute()
    {
        if ($this->image) {
            return Storage::url($this->image);
        }
        return null;
    }

    public function getImageVideoAttribute()
    {
        if ($this->cover_video) {
            return Storage::url($this->cover_video);
        }
        return null;
    }

    public function getImagesAttribute()
    {
        return $this->getMedia('developments')->map( function($mediaObject) {
            return $mediaObject->getFullUrl();
        });
        // if ($this->galery) {
        //     $images = json_decode($this->galery);
        //     foreach ($images as $key => $image) {
        //         $images[$key] = Storage::url($image);
        //     }
        //     return $images;
        // }
        // return null;
    }

    public function getBigImageAttribute()
    {
        if ($this->banner) {
            return Storage::url($this->banner);
        }
        return null;
    }

    public function getCoordinatesAttribute($value)
    {
        $coords = $this->location;
        if (!isset($coords->latitude) || !isset($coords->longitude)) {
            return sprintf("%s,%s", env('GOOGLE_MAPS_DEFAULT_CENTER_LAT'), env('GOOGLE_MAPS_DEFAULT_CENTER_LNG'));
        } else {
            return sprintf("%s,%s", $coords->latitude, $coords->longitude);
        }
    }

    public function getLocatorAttribute()
    {
        $coords = $this->location;
        
        if (isset($coords->latitude) && isset($coords->longitude)) {
            return json_encode([ 'lat' => $coords->latitude, 'lng' => $coords->longitude]);
        } else {
            return json_encode(['lat' => env('GOOGLE_MAPS_DEFAULT_CENTER_LAT'), 'lng' => env('GOOGLE_MAPS_DEFAULT_CENTER_LNG')]);
        }
    }

    public function saveLocation($location)
    {
        $parseLocation = preg_split ("/\,/", $location);
        $this->location = new Point($parseLocation[0], $parseLocation[1]);
        $this->save();
    }

    protected function serializeLocation($location)
    {
        $parseLocation = preg_split ("/\,/", $location);
        return  new Point($parseLocation[0], $parseLocation[1]);
    }

    public function getFileAttribute()
    {
        if (!empty($this->brochure)) {
           return Storage::url($this->brochure);
        } else {
            return null;
        }
    }

    public function getFilePriceAttribute()
    {
        if (!empty($this->price_file)) {
            return Storage::url($this->price_file);
        } else {
            return null;
        }
    }
}
