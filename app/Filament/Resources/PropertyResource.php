<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PropertyResource\Pages;
use App\Filament\Resources\PropertyResource\RelationManagers;
use App\Models\Property;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Livewire\TemporaryUploadedFile;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\BooleanSelect;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;

use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;

use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;

class PropertyResource extends Resource
{
    protected static ?string $model = Property::class;

    protected static ?string $title = 'Propiedades';

    protected static ?string $navigationLabel = 'Propiedades';

    protected static ?string $navigationIcon = 'heroicon-o-office-building';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()->schema([
                    Placeholder::make('Información general')
                        ->columnSpan(3),
                    Toggle::make('feature')
                        ->label('Destacar')
                        ->columnSpan(1),
                    TextInput::make('name')
                        ->label('Nombre')
                        ->required()
                        ->reactive()
                        ->afterStateUpdated(function (Closure $set, $get, $state) {
                            $set('slug', Str::slug($state));
                        })
                        ->columnSpan(2),
                    TextInput::make('slug')
                        ->required()
                        ->columnSpan(2),
                    TextInput::make('price')
                        ->label('Precio')
                        ->numeric()
                        ->mask(fn (TextInput\Mask $mask) => $mask
                            ->numeric()
                            ->decimalPlaces(2)
                            ->decimalSeparator(',')
                            ->thousandsSeparator(',')
                            ->mapToDecimalSeparator([','])
                        )->required()
                        ->columnSpan(2),
                    Select::make('currency')
                        ->label('Divisa')
                        ->relationship('currency', 'name')
                        ->required()
                        ->columnSpan(2),
                    TextInput::make('antique')
                        ->label('Antigüedad')
                        ->columnSpan(2),
                    TextInput::make('land_use')
                        ->label('Tipo de suelo')
                        ->columnSpan(2),
                    Select::make('tradeType')
                        ->label('Tipo de operación')
                        ->relationship('tradeType', 'name')
                        ->required()
                        ->columnSpan(2),
                    Select::make('propertyType')
                        ->label('Tipo de propiedad')
                        ->relationship('propertyType', 'name')
                        ->required()
                        ->columnSpan(2),
                    Select::make('location')
                        ->label('Ubicación')
                        ->relationship('location', 'address')
                        ->searchable()
                        ->required()
                        ->columnSpan(2),
                    TextInput::make('optioner')
                        ->label('Opcionador')
                        ->columnSpan(2),
                ])
                ->columns(4),

                Card::make()->schema([
                    TextInput::make('key_words')
                        ->label('Palabras clave'),
                ])
                ->columns(2),

                Card::make()->schema([
                    Placeholder::make('Descripción')
                        ->columnSpan(2),
                    RichEditor::make('short_description')
                        ->label('Descripción corta')
                        ->disableToolbarButtons([
                            'attachFiles',
                            'codeBlock',
                            'heading',
                            'subheading',
                        ])
                        ->columnSpan(2),
                    RichEditor::make('description')
                        ->label('Descripción larga')
                        ->disableToolbarButtons([
                            'attachFiles',
                            'codeBlock',
                            'heading',
                            'subheading',
                        ])
                        ->columnSpan(2),
                ])
                ->columns(2),

                Card::make()->schema([
                    Placeholder::make('Contacto')
                        ->columnSpan(2),
                    TextInput::make('contact')
                        ->label('Contacto'),
                    TextInput::make('visit_conditions')
                        ->label('Condiciones de visita'),
                    TextInput::make('phone')
                        ->label('Teléfono'),
                    TextInput::make('email')
                        ->label('Correo electrónico'),
                ])
                ->columns(2),

                Card::make()->schema([
                    Placeholder::make('M2')
                        ->columnSpan(4),
                    TextInput::make('land_square_meters')
                        ->label('Terreno')
                        ->numeric(),
                    TextInput::make('square_meters')
                        ->label('Metros cuadrados')
                        ->numeric(),
                    TextInput::make('front')
                        ->numeric()
                        ->label('Frente'),
                    TextInput::make('bottom')
                        ->numeric()
                        ->label('Fondo'),
                    TextInput::make('garden_square_meters')
                        ->numeric()
                        ->label('Metros de Jardín'),
                ])
                ->columns(4),

                Card::make()->schema([
                    Placeholder::make('Características')
                        ->columnSpan(4),
                    TextInput::make('floor')
                        ->label('Pisos')
                        ->numeric(),
                    TextInput::make('room')
                        ->label('Cuartos')
                        ->numeric(),
                    TextInput::make('bathroom')
                        ->label('Baños')
                        ->numeric(),
                    TextInput::make('car_spot')
                        ->label('Lugares de estacionamiento')
                        ->numeric(),
                    TextInput::make('room_service')
                        ->numeric()
                        ->label('Cuartos de servicio'),
                    TextInput::make('bathroom_service')
                        ->numeric()
                        ->label('Baños de servicio'),
                ])
                ->columns(4),

                Card::make()->schema([
                    Placeholder::make('Amenidades')
                        ->columnSpan(4),
                    Toggle::make('residential_development')
                        ->label('Coto'),
                    Toggle::make('furnished')
                        ->label('Amueblado'),
                    Toggle::make('vigilance')
                        ->label('Vigilancia'),
                    Toggle::make('terrace')
                        ->label('Terraza'),
                    Toggle::make('tv_salon')
                        ->label('Sala Television'),
                    Toggle::make('studio')
                        ->label('Estudio'),
                    Toggle::make('pet')
                        ->label('Mascotas'),
                    TextInput::make('other')
                        ->label('Otro')
                        ->columnSpan(2),
                    TextInput::make('architecture_type')
                        ->label('Tipo Arquitectura')
                        ->columnSpan(2),
                    TextInput::make('maintenance')
                        ->label('Mantenimiento')
                        ->columnSpan(2),
                    TextInput::make('nearby_service')
                        ->label('Servicios Cercanos')
                        ->columnSpan(2),
                    TextInput::make('colony_description')
                        ->label('Descripción Colonia ')
                        ->columnSpan(2),
                ])
                ->columns(4),

                Card::make()->schema([
                    Placeholder::make('Video')
                        ->columnSpan(4),
                    TextInput::make('youtube_video')
                        ->label('Youtube Video')
                        ->columnSpan(2),
                    FileUpload::make('cover_video')
                        ->label('Portada de video')
                        ->image()
                        ->columnSpan(2),
                    TextInput::make('virtual_tour')
                        ->label('Tour virtual')
                        ->columnSpan(2),
                ])
                ->columns(4),

                Section::make('Galería')
                    ->schema([
                        SpatieMediaLibraryFileUpload::make('images')
                            ->label('Imagenes')
                            ->multiple()
                            ->enableReordering()
                            ->responsiveImages()
                            ->collection('properties')
                            ->columnSpan(2),
                    ])
                    ->collapsible()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                    ->label('ID')
                    ->sortable(),
                TextColumn::make('name')
                    ->label('Nombre')
                    ->sortable()
                    ->searchable(),
                TextColumn::make('price')
                    ->label('Precio')
                    ->money('mxn')
                    ->sortable(),
                TextColumn::make('currency.name')
                    ->label('Divisa')
                    ->sortable(),
                TextColumn::make('tradeType.name')
                    ->label('Tipo de operación')
                    ->sortable(),
                TextColumn::make('propertyType.name')
                    ->label('Tipo de propiedad')
                    ->sortable(),
                TextColumn::make('land_use')
                    ->label('Tipo de suelo')
                    ->sortable(),
                TextColumn::make('antique')
                    ->label('Antigüedad')
                    ->sortable(),
                TextColumn::make('optioner')
                    ->label('Opcionador')
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                ExportBulkAction::make(),
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProperties::route('/'),
            'create' => Pages\CreateProperty::route('/create'),
            'edit' => Pages\EditProperty::route('/{record}/edit'),
        ];
    }
}
