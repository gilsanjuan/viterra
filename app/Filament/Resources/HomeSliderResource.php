<?php

namespace App\Filament\Resources;

use App\Filament\Resources\HomeSliderResource\Pages;
use App\Filament\Resources\HomeSliderResource\RelationManagers;
use App\Models\HomeSlideImage;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Fieldset;

use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ToggleColumn;

class HomeSliderResource extends Resource
{
    protected static ?string $model = HomeSlideImage::class;

    protected static ?string $title = 'Home Slider';

    protected static ?string $navigationLabel = 'Home Sliders';

    protected static ?string $navigationIcon = 'heroicon-o-photograph';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()->schema([
                    Placeholder::make('Slide')
                        ->columnSpan(3),
                    TextInput::make('name')
                        ->label('Nombre')
                        ->required()
                        ->columnSpan(1),
                    TextInput::make('position')
                        ->numeric()
                        ->minValue(1)
                        ->required()
                        ->columnSpan(1),
                    Fieldset::make('Mostrar')
                        ->schema([
                            Toggle::make('isActive')
                            ->label('Activo')
                        ])
                        ->columnSpan(1),
                    FileUpload::make('url')
                        ->label('Imagen')
                        ->image()
                        ->directory('home-sliders')
                        ->columnSpan(3),
                ])->columns(3),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                    ->label('Nombre')
                    ->sortable()
                    ->searchable(),
                ImageColumn::make('url')
                    ->label('Imagen'),
                TextColumn::make('position')
                    ->label('Posición')
                    ->sortable()
                    ->searchable(),
                ToggleColumn::make('isActive')
                    ->label('Activo')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListHomeSliders::route('/'),
            'create' => Pages\CreateHomeSlider::route('/create'),
            'edit' => Pages\EditHomeSlider::route('/{record}/edit'),
        ];
    }
}
