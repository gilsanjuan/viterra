<?php

namespace App\Filament\Resources;

use Filament\Resources\Concerns\Translatable;
use App\Filament\Resources\TradeTypeResource\Pages;
use App\Filament\Resources\TradeTypeResource\RelationManagers;
use App\Models\TradeType;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class TradeTypeResource extends Resource
{
    use Translatable;

    protected static ?string $model = TradeType::class;

    protected static ?string $title = 'Tipos de comercio';

    protected static ?string $navigationLabel = 'Tipos de comercio';

    protected static ?string $navigationIcon = 'heroicon-o-switch-horizontal';

    protected static ?int $navigationSort = 7;

    public static function getTranslatableLocales(): array
    {
        return ['es', 'en'];
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label('Nombre')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label('Nombre')
                    ->sortable()
                    ->searchable(),
                //
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTradeTypes::route('/'),
            'create' => Pages\CreateTradeType::route('/create'),
            'edit' => Pages\EditTradeType::route('/{record}/edit'),
        ];
    }
}
