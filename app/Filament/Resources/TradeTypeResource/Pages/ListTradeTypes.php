<?php

namespace App\Filament\Resources\TradeTypeResource\Pages;

use App\Filament\Resources\TradeTypeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTradeTypes extends ListRecords
{
    use ListRecords\Concerns\Translatable;

    protected static string $resource = TradeTypeResource::class;

    protected function getActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
            Actions\CreateAction::make(),
        ];
    }
}
