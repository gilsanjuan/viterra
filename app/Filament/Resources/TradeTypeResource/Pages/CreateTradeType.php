<?php

namespace App\Filament\Resources\TradeTypeResource\Pages;

use App\Filament\Resources\TradeTypeResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTradeType extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;

    protected static string $resource = TradeTypeResource::class;

    protected function getActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
        ];
    }
}
