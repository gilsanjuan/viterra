<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CityResource\Pages;
use App\Filament\Resources\CityResource\RelationManagers;
use App\Models\City;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Placeholder;

use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\BadgeColumn;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;


class CityResource extends Resource
{
    protected static ?string $model = City::class;

    protected static ?string $title = 'Ciudades';

    protected static ?string $navigationLabel = 'Ciudades';

    protected static ?string $navigationIcon = 'heroicon-o-location-marker';

    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()->schema([
                    Placeholder::make('Información general')
                        ->columnSpan(3),
                    Toggle::make('is_active')
                            ->label('Visible')
                            ->columnSpan(1),
                    TextInput::make('name')
                        ->label('Nombre')
                        ->required()
                        ->columnSpan(2),
                ])
                ->columns(4),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                    ->label('Nombre')
                    ->sortable()
                    ->searchable(),
                BadgeColumn::make('is_active')
                    ->label('Es visible')
                    ->sortable()
                    ->enum([
                        0 => 'No',
                        1 => 'Si',
                    ])
                    ->color(static function ($state): string {
                        if ($state === 1) {
                            return 'success';
                        }

                        return 'warning';
                    })
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCities::route('/'),
            'create' => Pages\CreateCity::route('/create'),
            'edit' => Pages\EditCity::route('/{record}/edit'),
        ];
    }
}
