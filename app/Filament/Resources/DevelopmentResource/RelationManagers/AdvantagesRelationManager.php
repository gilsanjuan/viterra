<?php

namespace App\Filament\Resources\DevelopmentResource\RelationManagers;

use Filament\Forms;
use Filament\Tables;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\RelationManagers\RelationManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class AdvantagesRelationManager extends RelationManager
{
    protected static string $relationship = 'advantage';

    protected static ?string $recordTitleAttribute = 'advantage_id';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\ComponentsTextInput::make('name')
                    ->label('Nombre')
                    ->required(),
                Forms\ComponentsFileUpload::make('icon')
                    ->label('Icono')
                    ->image()
                    ->directory('advantages')
                    ->required()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label('Nombre')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\ImageColumn::make('icon')
                    ->label('Icono')
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
}
