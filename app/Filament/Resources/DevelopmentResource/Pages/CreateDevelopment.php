<?php

namespace App\Filament\Resources\DevelopmentResource\Pages;

use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
use MatanYadaev\EloquentSpatial\Objects\Point;
use App\Filament\Resources\DevelopmentResource;

class CreateDevelopment extends CreateRecord
{
    protected static string $resource = DevelopmentResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        if(isset($data['locator'])) {
            $location = $data['locator'];
            $decoded = json_decode($location);
            if (isset($decoded->lat) && isset($decoded->lng)) {
                $data['location'] = new Point($decoded->lat, $decoded->lng);
            }
        }

        return $data;
    }
}
