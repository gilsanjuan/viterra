<?php

namespace App\Filament\Resources\DevelopmentResource\Pages;

use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use MatanYadaev\EloquentSpatial\Objects\Point;
use App\Filament\Resources\DevelopmentResource;

class EditDevelopment extends EditRecord
{
    protected static string $resource = DevelopmentResource::class;

    protected function mutateFormDataBeforeSave(array $data): array
    {
        $location = $data['locator'];
        $decoded = json_decode($location);
        if (isset($decoded->lat) && isset($decoded->lng)) {
            $data['location'] = new Point($decoded->lat, $decoded->lng);
        }

        return $data;
    }

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
