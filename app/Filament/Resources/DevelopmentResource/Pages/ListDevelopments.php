<?php

namespace App\Filament\Resources\DevelopmentResource\Pages;

use App\Filament\Resources\DevelopmentResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListDevelopments extends ListRecords
{
    protected static string $resource = DevelopmentResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
