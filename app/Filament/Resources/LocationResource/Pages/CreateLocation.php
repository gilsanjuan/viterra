<?php

namespace App\Filament\Resources\LocationResource\Pages;

use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
use App\Filament\Resources\LocationResource;
use MatanYadaev\EloquentSpatial\Objects\Point;

class CreateLocation extends CreateRecord
{
    protected static string $resource = LocationResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        if(isset($data['locator'])) {
            $location = $data['locator'];
            $decoded = json_decode($location);
            if (isset($decoded->lat) && isset($decoded->lng)) {
                $data['lat_lng'] = new Point($decoded->lat, $decoded->lng);
            }
        }

        return $data;
    }
}
