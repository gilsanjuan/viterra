<?php

namespace App\Filament\Resources\LocationResource\Pages;

use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use App\Filament\Resources\LocationResource;
use MatanYadaev\EloquentSpatial\Objects\Point;

class EditLocation extends EditRecord
{
    protected static string $resource = LocationResource::class;

    protected function mutateFormDataBeforeSave(array $data): array
    {
        $location = $data['locator'];
        $decoded = json_decode($location);
        if (isset($decoded->lat) && isset($decoded->lng)) {
            $data['lat_lng'] = new Point($decoded->lat, $decoded->lng);
        }

        return $data;
    }

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
