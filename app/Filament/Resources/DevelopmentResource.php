<?php

namespace App\Filament\Resources;

use App\Filament\Resources\DevelopmentResource\Pages;
use App\Filament\Resources\DevelopmentResource\RelationManagers;
use App\Models\Development;

use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

use Closure;
use Illuminate\Support\Str;
use Livewire\TemporaryUploadedFile;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;

use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;

use Yemenpoint\FilamentGoogleMapLocationPicker\Forms\Components\LocationPicker;

class DevelopmentResource extends Resource
{
    protected static ?string $model = Development::class;

    protected static ?string $title = 'Desarrollos';

    protected static ?string $navigationLabel = 'Desarrollos';

    protected static ?string $navigationIcon = 'heroicon-o-briefcase';

    protected static ?int $navigationSort = 2;

    protected $queryString = [
        'tableColumnSearchQueries',
    ];

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()->schema([
                    Placeholder::make('Información general')
                        ->columnSpan(2),
                    TextInput::make('name')
                        ->label('Nombre')
                        ->required()
                        ->reactive()
                        ->afterStateUpdated(function (Closure $set, $get, $state) {
                            $set('slug', Str::slug($state));
                        }),
                    TextInput::make('slug')
                        ->required(),
                    TextInput::make('price')
                        ->label('Precio')
                        ->numeric()
                        ->mask(fn (TextInput\Mask $mask) => $mask
                            ->numeric()
                            ->decimalPlaces(2)
                            ->decimalSeparator(',')
                            ->thousandsSeparator(',')
                            ->mapToDecimalSeparator([','])
                        )->required(),
                    Select::make('currency')
                        ->label('Divisa')
                        ->relationship('currency', 'name')
                        ->required(),
                    TextInput::make('address')
                        ->label('Dirección')
                        ->required(),
                    Select::make('advantage_id')
                        ->label('Ventajas')
                        ->multiple()
                        ->relationship('advantages', 'name')
                        ->options(
                            \App\Models\Advantage::all()->pluck('name', 'id')
                        ),
                    // TextInput::make('location')
                    //     ->label('Ubicación')
                    //     ->afterStateHydrated(function (TextInput $component, $record, $state) {
                    //         if(!isset($record->location)) {
                    //             $component->state(
                    //                 sprintf("%s,%s", env('GOOGLE_MAPS_DEFAULT_CENTER_LAT'), env('GOOGLE_MAPS_DEFAULT_CENTER_LNG'))
                    //             );
                    //         } else {
                    //             if (isset($record->location->latitude) || isset($record->location->longitude)) {
                    //                 $component->state(
                    //                     sprintf("%s,%s", $record->location->latitude, $record->location->longitude)
                    //                 );
                    //             }
                    //         }
                    //     }),
                    RichEditor::make('description')
                        ->label('Descripción')
                        ->disableToolbarButtons([
                            'attachFiles',
                            'codeBlock',
                            'heading',
                            'subheading',
                        ])
                        ->required()
                        ->columnSpan(2),
                ])->columns(2),

                Card::make()
                    ->schema([
                        Placeholder::make('Ubicación')
                            ->columnSpan(2),
                        LocationPicker::make('locator')
                            ->label('ubicación')
                            // ->default(json_encode(["lat" => 20.63968, "lng" => -103.47341]))//set default location
                            ->defaultZoom(12)// set zoom 
                            ->setLocationCenter([
                                'lat' => 20.65901034175825, 
                                'lng' =>  -103.3520649132686
                            ]) //set location center 
                            // ->isSearchBoxControlEnabled()
                            ->required()
                            ->mapControls([
                                'mapTypeControl' => true,
                                'scaleControl' => true,
                                'streetViewControl' => true,
                                'rotateControl' => true,
                                'fullscreenControl' => true,
                                'zoomControl' => true,
                                'searchBoxControl' => true
                            ])
                            ->columnSpan(2),
                ])->columns(2),

                Card::make()
                    ->schema([
                        Placeholder::make('Imágenes')
                            ->columnSpan(2),
                        FileUpload::make('image')
                            ->label('Logo')
                            ->image(),
                        FileUpload::make('banner')
                            ->image(),
                ])->columns(2),

                Card::make()->schema([
                    Placeholder::make('Documentos')
                    ->columnSpan(2),
                    FileUpload::make('brochure')
                        ->directory('developments')
                        ->getUploadedFileNameForStorageUsing(function (TemporaryUploadedFile $file): string {
                            $name = explode('.pdf', $file->getClientOriginalName());
                            return (string) str(Str::slug($name[0]). '.pdf');
                        })
                        ->acceptedFileTypes(['application/pdf']),
                    FileUpload::make('price_file')
                        ->label('Lista de precios')
                        ->directory('developments')
                        ->getUploadedFileNameForStorageUsing(function (TemporaryUploadedFile $file): string {
                            $name = explode('.pdf', $file->getClientOriginalName());
                            return (string) str(Str::slug($name[0]). '.pdf');
                        })
                        ->acceptedFileTypes(['application/pdf']),
                ])->columns(2),

                Card::make()->schema([
                    Placeholder::make('Video')
                    ->columnSpan(2),
                    TextInput::make('youtube_video')
                        ->label('Video en youtube'),
                    FileUpload::make('cover_video')
                        ->label('Portada de video')
                        ->image(),
                ])->columns(2),

                Section::make('Galería')
                    ->schema([
                        SpatieMediaLibraryFileUpload::make('images')
                            ->label('Imagenes')
                            ->multiple()
                            ->enableReordering()
                            ->responsiveImages()
                            ->collection('developments')
                            ->columnSpan(2),
                ])->collapsible(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                ImageColumn::make('image')
                ->label('Logo'),
                TextColumn::make('name')
                    ->label('Nombre')
                    ->sortable()
                    ->searchable(),
                TextColumn::make('price')
                    ->label('Precio')
                    ->money('mxn')
                    ->sortable(),
                TextColumn::make('currency.name')
                    ->label('Divisa')
                    ->sortable(),
                TextColumn::make('address')
                    ->label('Dirección')->limit(30),
                //
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDevelopments::route('/'),
            'create' => Pages\CreateDevelopment::route('/create'),
            'edit' => Pages\EditDevelopment::route('/{record}/edit'),
        ];
    }
}
