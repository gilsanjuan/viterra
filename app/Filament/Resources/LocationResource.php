<?php

namespace App\Filament\Resources;

use App\Filament\Resources\LocationResource\Pages;
use App\Filament\Resources\LocationResource\RelationManagers;
use App\Models\Location;

use Spatie\Geocoder\Geocoder;

use Closure;

use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;


use Filament\Forms\Components\Card;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Placeholder;

use Filament\Tables\Columns\TextColumn;

use Yemenpoint\FilamentGoogleMapLocationPicker\Forms\Components\LocationPicker;

class LocationResource extends Resource
{
    protected static ?string $model = Location::class;

    protected static ?string $title = 'Ubicaciones';

    protected static ?string $navigationLabel = 'Ubicaciones';

    protected static ?string $navigationIcon = 'heroicon-o-location-marker';

    protected static ?int $navigationSort = 1;

    protected $queryString = [
        'tableColumnSearchQueries',
    ];

    protected static function getLanLng($query) {
        $client = new \GuzzleHttp\Client();
        $geocoder = new Geocoder($client);
        $geocoder->setApiKey(config('geocoder.key'));
        $response = $geocoder->getCoordinatesForAddress($query);
        // $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
        //     'query' => [
        //         'address' => $query,
        //         'key' => env('GOOGLE_MAPS_API_KEY'),
        //     ]
        // ]);
        return $response;
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()->schema([
                    Placeholder::make('Información general')
                        ->columnSpan(4),
                    // Select::make('geocoding')
                    //     ->reactive()
                    //     ->searchable()
                    //     ->dehydrated(false)
                    //     ->label('Buscar coordenadas')
                    //     ->getSearchResultsUsing(function ($query) {
                    //         if ($query) {
                    //             $res = LocationResource::getLanLng($query);
                    //             return [
                    //                 $res['lat'] . ',' . $res['lng'] => $res['formatted_address'],
                    //             ];
                    //         }
                    //     })
                    //     ->afterStateUpdated(function ($state, $set) {
                    //         if($state) {
                    //             /** @var \Geocoder\Provider\GoogleMaps\Model\GoogleAddress $result */
                    //             $set('location', '[]');
                    //             if (isset($state)) {
                    //                 $latlng = explode(",", $state);
                    //                 if (count($latlng)) {
                    //                     // dd($latlng);
                    //                     $set('location', ["lat" =>  $latlng[0], "lng" => $latlng[1]] );      
                    //                 }
                    //             }
                    //         }
                    //     })
                    //     ->columnSpan(3),
                    // TextInput::make('lat_lng')
                    //     ->label('Coordenadas')
                        // ->afterStateHydrated(function (TextInput $component, $record, $state) {
                        //     if (isset($record->lat_lng->latitude) || isset($record->lat_lng->longitude)) {
                        //         $component->state(
                        //             sprintf("%s,%s", $record->lat_lng->latitude, $record->lat_lng->longitude)
                        //         );
                        //     }
                        //     if ($state) {
                        //         dd($state);
                        //     }
                        //     // dd('hy',$state);
                        // })
                        // ->afterStateUpdated(function (Closure $set, $state) {
                        //     if($state && is_array($state)) {
                        //         $firstKey = array_key_first($state);
                        //         if (array_key_exists('lat', $state)) {
                        //             $lat = $state[$firstKey]['lat'];
                        //             $lng = $state[$firstKey]['lng'];
                        //             $set['lat_lng'] = $lat . ', ' . $lng;
                        //         }
                        //         // dd($state);
                        //     }
                        //     // $set('slug', Str::slug($state));
                        // })            
                        //  ->required()
                        // ->columnSpan(1),
                    TextInput::make('address')
                        ->label('Dirección')
                        ->required()
                        ->columnSpan(4),
                    TextInput::make('street')
                        ->label('Calle')
                        ->columnSpan(1),
                    TextInput::make('suburb')
                        ->label('Colonia')
                        ->columnSpan(1),
                    TextInput::make('no_ext')
                        ->label('No. exterior')
                        ->columnSpan(1),
                    TextInput::make('no_int')
                        ->label('No. interior')
                        ->columnSpan(1),
                    Select::make('city')
                        ->label('Ciudad')
                        ->relationship('city', 'name')
                        ->required()
                        ->columnSpan(1),
                    TextInput::make('town')
                        ->label('Municipio')
                        ->columnSpan(1),
                    TextInput::make('zip')
                        ->label('Código postal')
                        ->columnSpan(1),

                    // LocationPicker::make('location')
                    //     // ->default(json_encode(["lat" => 20.660777184199894, "lng" => -103.34931840069196]))//set default location
                    //     ->defaultZoom(6)// set zoom 
                    //     ->setLocationCenter([
                    //         'lat' => 20.660777184199894,
                    //         'lng' => -103.34931840069196,
                    //     ])
                    //     ->columnSpan(4),

                    LocationPicker::make('locator')
                        ->label('ubicación')
                        // ->default(json_encode(["lat" => 20.63968, "lng" => -103.47341]))//set default location
                        ->defaultZoom(12)// set zoom 
                        ->setLocationCenter([
                            'lat' => 20.65901034175825, 
                            'lng' =>  -103.3520649132686
                        ]) //set location center 
                        // ->isSearchBoxControlEnabled()
                        ->required()
                        ->mapControls([
                            'mapTypeControl' => true,
                            'scaleControl' => true,
                            'streetViewControl' => true,
                            'rotateControl' => true,
                            'fullscreenControl' => true,
                            'zoomControl' => true,
                            'searchBoxControl' => true
                        ])
                        ->columnSpan(4),
                ])->columns(4),
                //
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('id')
                    ->label('ID')
                    ->sortable(),
                TextColumn::make('address')
                    ->label('Dirección')
                    ->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListLocations::route('/'),
            'create' => Pages\CreateLocation::route('/create'),
            'edit' => Pages\EditLocation::route('/{record}/edit'),
        ];
    }    
}
