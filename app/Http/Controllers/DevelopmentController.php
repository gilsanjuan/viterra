<?php

namespace App\Http\Controllers;

use App\Models\Development;
use Illuminate\Http\Request;

class DevelopmentController extends Controller
{
    public function index()
    {
        return Development::with('currency')->paginate(4);
    }
    //
    public function getFeatured()
    {
        $developments = Development::with('currency')->inRandomOrder()
            ->get()->makeHidden(['created_at', 'updated_at']);

        if ($developments->count()) {
            return $developments;
        } else {
            return response()->json(['Error al buscar propiedades'], 500);
        }
    }

    public function show(Request $request)
    {
        $development = Development::where('slug', $request->slug)
            ->with('advantages')->with('currency')->get()->first();

        if ($development->count()) {
            $development->append('images');
            return $development;
        } else {
            return response()->json(['No se encontro el desarollo'], 500);
        }
    }
}
