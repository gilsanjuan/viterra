<?php

namespace App\Http\Controllers;

use App\Models\PropertyType;
use Illuminate\Http\Request;

class PropertyTypeController extends Controller
{
    public function getPropertyTypes()
    {
        try {
            $propertyTypes = PropertyType::get();
        } catch (\Exception $e) {
            return response()->json(['Hubo un error al resolver la peticion'], 500);
        }

        return response()->json($propertyTypes);
    }
}
