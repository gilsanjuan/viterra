<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    //
    public function getCities()
    {
        try {
            $cities = City::all()->where('is_active', 1)->sortBy('name');
        } catch (\Exception $e) {
            return response()->json(['no se encontraron ciudades'],400);
        }

        return response()->json($cities);
    }
}
