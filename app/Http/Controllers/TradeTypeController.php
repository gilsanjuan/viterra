<?php

namespace App\Http\Controllers;

use App\Models\TradeType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TradeTypeController extends Controller
{
    public function getTradeTypes()
    {
        try {
            // $tradeTypes = TradeType::withTranslation('en')->get();
            $tradeTypes = TradeType::get();
        } catch (\Exception $e) {
            return response()->json(['Hubo un error al resolver la peticion'], 500);
        }

        return response()->json($tradeTypes);
    }
}
