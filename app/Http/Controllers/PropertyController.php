<?php

namespace App\Http\Controllers;

use Dompdf\Exception;
use http\Env\Response;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class PropertyController extends Controller
{
    public function getProperties() {
        $properties = Property::with('location')->with('currency')->get()->makeHidden(['created_at', 'updated_at']);

        if ($properties->count()) {
            $properties->append('images');
            return response()->json($properties);
        } else {
            return response()->json(['No se encontro esa propiedad'], 500);
        }
    }

    public function filterProperties(Request $request) {

        $validator = Validator::make($request->all(), [
            'filter' => 'required|array',
            'place' => 'string|nullable',
            'tradeType' => 'integer|nullable|exists:trade_types,id',
            'propertyType' => 'integer|nullable|exists:property_types,id'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (empty($request->place) && empty($request->tradeType) && empty($request->propertyType)) {
            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->whereBetween('price', $request->filter)->orderby('name')
                ->paginate(20);

        } else if (!empty($request->place) && empty($request->tradeType) && empty($request->propertyType)) {
            $ids = Property::where('name', 'like', "%$request->place%")->orWhereHas('location', function (Builder $query) use ($request) {
                $query->where('address', 'like', "%$request->place%")->orWhereHas('city', function (Builder $query) use ($request) {
                    $query->where('name', 'like', "%$request->place%");
                });
            })->select('id')->get()->toArray();

            foreach ($ids as $key => $id) {
                $ids[$key] = $id['id'];
            }

            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->whereIn('id', $ids)->whereBetween('price', $request->filter)
                ->orderby('name')->paginate(20);

        } else if (empty($request->place) && !empty($request->tradeType) && empty($request->propertyType)) {
            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->where('trade_type_id', '=', $request->tradeType)
                ->whereBetween('price', $request->filter)->orderby('name')->paginate(20);

        } else if (empty($request->place) && empty($request->tradeType) && !empty($request->propertyType)) {
            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->where('property_type_id', '=', $request->propertyType)
                ->whereBetween('price', $request->filter)->orderby('name')->paginate(20);

        } else if (empty($request->place) && !empty($request->tradeType) && !empty($request->propertyType)) {
            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->where([
                    ['property_type_id', '=', $request->propertyType],
                    ['trade_type_id', '=', $request->tradeType],
                ])->whereBetween('price', $request->filter)->orderby('name')->paginate(20);

        } else if (!empty($request->place) && !empty($request->tradeType) && empty($request->propertyType)) {
            $ids = Property::where('name', 'like', "%$request->place%")->orWhereHas('location', function (Builder $query) use ($request) {
                $query->where('address', 'like', "%$request->place%")->orWhereHas('city', function (Builder $query) use ($request) {
                    $query->where('name', 'like', "%$request->place%");
                });
            })->select('id')->get()->toArray();

            foreach ($ids as $key => $id) {
                $ids[$key] = $id['id'];
            }

            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->where('trade_type_id', '=', $request->tradeType)
                ->whereIn('id', $ids)->whereBetween('price', $request->filter)->orderby('name')
                ->paginate(20);

        } else if (!empty($request->place) && empty($request->tradeType) && !empty($request->propertyType)) {
            $ids = Property::where('name', 'like', "%$request->place%")->orWhereHas('location', function (Builder $query) use ($request) {
                $query->where('address', 'like', "%$request->place%")->orWhereHas('city', function (Builder $query) use ($request) {
                    $query->where('name', 'like', "%$request->place%");
                });
            })->select('id')->get()->toArray();

            foreach ($ids as $key => $id) {
                $ids[$key] = $id['id'];
            }

            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->where('property_type_id', '=', $request->propertyType)
                ->whereIn('id', $ids)->whereBetween('price', $request->filter)->orderby('name')
                ->paginate(20);

        } else if (!empty($request->place) && !empty($request->tradeType) && !empty($request->propertyType)) {
            $ids = Property::where('name', 'like', "%$request->place%")->orWhereHas('location', function (Builder $query) use ($request) {
                $query->where('address', 'like', "%$request->place%")->orWhereHas('city', function (Builder $query) use ($request) {
                    $query->where('name', 'like', "%$request->place%");
                });
            })->select('id')->get()->toArray();

            foreach ($ids as $key => $id) {
                $ids[$key] = $id['id'];
            }

            $properties = Property::with('location')->with('currency')->with('tradeType')
                ->with('propertyType')->where([['property_type_id', $request->propertyType],
                    ['trade_type_id', $request->tradeType]])->whereIn('id', $ids)
                ->whereBetween('price', $request->filter)->orderby('name')->paginate(20);

        }

        if ($properties->count()) {
            $properties->append('images');
            return response()->json($properties);
        } else {
            return response()->json(['Error al buscar propiedades'], 500);
        }
    }

    public function getMoney() {
        $money = Property::max('price');
        $money = intval($money) + 2000;

        if (empty($money)) {
            return response()->json(['Error al encontrar el maximo'], 500);
        } else {
            return response()->json($money);
        }
    }

    public function getPropertiesCity(Request $request) {
        $ids = Property::join('locations', 'properties.location_id', '=', 'locations.id')
            ->join('cities', 'cities.id', '=', 'locations.city_id')->where('cities.name', 'like', "%$request->place%")
            ->select('properties.id')->get()->toArray();

        foreach ($ids as $key => $id) {
            $ids[$key] = $id['id'];
        }

        $properties = Property::with('location')->with('currency')->with('tradeType')
            ->with('propertyType')->whereIn('id', $ids)->where('price', '>', $request->filter[0])
            ->where('price', '<', $request->filter[1])->orderby('name')
            ->paginate(20);


        if ($properties->count()) {
            $properties->append('images');
            return response()->json($properties);
        } else {
            return response()->json(['Error al buscar propiedades'], 500);
        }
    }

    public function getFeatured() {
        $properties = Property::with('location')->with('currency')
            ->where('feature', true)->inRandomOrder()->take(10)
            ->get()->makeHidden(['created_at', 'updated_at']);

        if ($properties->count()) {
            $properties->append('images');
            return response()->json($properties);
        } else {
            return response()->json(['Error al buscar propiedades'], 500);
        }
    }

    public function downloadPDF($id) {
        try {
            $property = Property::with('location')->find($id);
            $view = \View::make('pdf.property', compact('property'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            return $pdf->download("$property->name.pdf");
        } catch (Exception $e) {
            \Log::error($e);
            return abort(500);
        }
    }

    public function show(Request $request) {
        $validator = Validator::make($request->all(), [
            'slug' => 'required|alpha_dash|exists:properties,slug',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $property = Property::with('location')->with('currency')->with('tradeType')->with('propertyType')
        ->where('slug', $request->slug)->get()->first();

        if ($property->count()) {
            $property->append('images');
            return $property;
        } else {
            return response()->json(['No se encontro esa propiedad'], 500);
        }
    }
}
