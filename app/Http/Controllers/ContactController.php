<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:100',
            'phone' => 'required|integer|min:8',
            'comment' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try
        {
            $contact = new Contact($request->all());
            $contact->save();
            return response()->json(['success'], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['Error al guardar'], 500);
        }
    }
}
