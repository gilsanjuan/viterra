<?php

namespace App\Http\Controllers;

use App\Models\HomeSlideImage;
use Illuminate\Http\Request;

class HomeSlideImageController extends Controller
{
    public function index() {
        try {
            $images = HomeSlideImage::where('isActive', 1)->select('url')->orderBy('position')->get();
        } catch (\Exception $e) {
            return response()->json([],400);
        }

        return response()->json($images);
    }
}
