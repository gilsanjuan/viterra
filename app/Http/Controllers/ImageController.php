<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function update(Request $request, Image $image)
    {
        $input=$request->all();
        $data = collect(json_decode($input['order']));

        $id = json_decode($input['order'],true)[0]['id'];
        $field =json_decode($input['order'],true)[0]['fieldName'];

        $images = $data->pluck("image");

        $item = Image::where("id", $id)->firstOrFail();
        $item->name = json_encode($images);
        $item->update();
    }
}
